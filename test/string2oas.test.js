/* eslint-env mocha */

const string2oas = require('../lib/string2oas')
const Joi = require('@hapi/joi')

const basicPatternCases = {
  'alpha-numeric': { yep: 'tHisiSaBasIcCAse0123456', nope: 'THis 993 Should **NOT** work' },
  token: { yep: 'tHisiSaBasIc_CAse0123456', nope: 'THis 993 Should _ **NOT** work' },
  email: { yep: 'Jan_Dockx@peopleware.be', nope: 'Jan_Dockx@ppw' },
  ipv4: { yep: '192.168.0.1', nope: '192.168.0.1/23' },
  'ipv4-cidr': { yep: '192.168.0.1/23', nope: '192.168.0.1' },
  ipv6: { yep: '2001:db8:a0b:12f0::1', nope: '2001:db8:a0g:12f0::1' },
  'ipv6-cidr': { yep: '2001:db8:a0b:12f0::1/122', nope: '2001:db8:a0b:12f0::1/129' },
  uri: {
    yep: 'https://snipplr.com/view/6889/regular-expressions-for-uri-validationparsing/',
    nope: 'https://snipplr.com/view/6889/regular-expressions-for>>uri-validationparsing/'
  },
  guid: { yep: '{f27a5b7f-0b7c-4e79-aff5-bdb0d34f3a9f}', nope: 'f27a5b7f-0b7c-4e79-aff5-bdb0d34f3a9f' },
  uuidv1: { yep: '12c65820-9b23-11e9-94cb-9775fa4f102f', nope: '2489a6bz-2941-4b63-a552-60c05c70ee1d' },
  uuidv2: { yep: '12c65820-9b23-11e9-94cb-9775fa4f102f', nope: '' },
  uuidv3: { yep: '2489a6bb-2941-4b63-a552-60c05c70ee1d', nope: '2489a6bz-2941-4b63-a552-60c05c70ee1dd' },
  uuidv4: { yep: '2489a6bb-2941-4b63-a552-60c05c70ee1d', nope: '2489a6bz-2941-4b63e-a552-60c05c70ee1d' },
  uuidv5: { yep: '2489a6bb-2941-4b63-a552-60c05c70ee1d', nope: '2489a6bz-2941-4b63-a552-60c05c70ee1' },
  hexadecimal: { yep: '894ABFed99', nope: '894A BFed99' },
  base64: { yep: 'ejJlQUVFSFpCcUpjVDNlWW5WcEd0QQ==', nope: 'ejJlQUVFSFpCcUpjVDNlWW5WcEd0QQ' },
  'data-uri': {
    // example from https://www.npmjs.com/package/data-uri-regex
    yep:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==',
    nope:
      'data:image/png;base65,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=='
  },
  hostname: { yep: 'www.example.org', nope: 'www_.example.org' },
  trimmed: { yep: 'This text is trimmed', nope: 'This text is not trimmed\t' },
  'iso-8601': { yep: new Date(2019, 5, 30, 14, 44, 9, 993).toISOString(), nope: '2019-06-32T14:44:09.993Z' }
}

describe('string2oas', function () {
  describe('patterns', function () {
    Object.keys(string2oas.patterns).forEach(p => {
      describe(p, function () {
        it(`${p} can be interpreted as a RegExp`, function () {
          const re = new RegExp(string2oas.patterns[p])
          re.should.be.an.instanceof(RegExp)
        })
        describe('smoke', function () {
          before(function () {
            this.re = new RegExp(string2oas.patterns[p])
          })

          after(function () {
            delete this.re
          })

          it(`${p} matches a basic matching pattern`, function () {
            console.log(`re: ${this.re}`)
            console.log(`string: ${basicPatternCases[p].yep}`)
            this.re.test(basicPatternCases[p].yep).should.be.ok()
          })

          it(`${p} does not match a basic non-matching pattern`, function () {
            console.log(`re: ${this.re}`)
            console.log(`string: ${basicPatternCases[p].nope}`)
            this.re.test(basicPatternCases[p].nope).should.not.be.ok()
          })
        })
      })
    })
  })

  describe('string2oas', function () {
    beforeEach(function () {
      string2oas.contract.verifyPostconditions = true
    })

    afterEach(function () {
      string2oas.contract.verifyPostconditions = false
    })

    const simpleSchemata = [
      Joi.string(),
      Joi.string().creditCard(),
      Joi.string()
        .creditCard()
        .creditCard(),
      Joi.string().alphanum(),
      Joi.string().token(),
      Joi.string().email(), // TODO options
      Joi.string().ip(),
      Joi.string().ip({ version: 'ipv4' }),
      Joi.string().ip({ version: 'ipv6' }),
      Joi.string().ip({ version: 'ipvfuture' }),
      Joi.string().ip({ cidr: 'optional' }),
      Joi.string().ip({ version: 'ipv4', cidr: 'optional' }),
      Joi.string().ip({ version: 'ipv6', cidr: 'optional' }),
      Joi.string().ip({ version: 'ipvfuture', cidr: 'optional' }),
      Joi.string().ip({ cidr: 'required' }),
      Joi.string().ip({ version: 'ipv4', cidr: 'required' }),
      Joi.string().ip({ version: 'ipv6', cidr: 'required' }),
      Joi.string().ip({ version: 'ipvfuture', cidr: 'required' }),
      Joi.string().ip({ cidr: 'forbidden' }),
      Joi.string().ip({ version: 'ipv4', cidr: 'forbidden' }),
      Joi.string().ip({ version: 'ipv6', cidr: 'forbidden' }),
      Joi.string().ip({ version: 'ipvfuture', cidr: 'forbidden' }),
      Joi.string().ip({ version: ['ipv4', 'ipv6', 'ipvfuture'], cidr: 'forbidden' }),
      Joi.string().uri(), // TODO options
      Joi.string().guid(),
      Joi.string().uuid(),
      Joi.string().guid({ version: 'uuidv1' }),
      Joi.string().uuid({ version: 'uuidv1' }),
      Joi.string().guid({ version: 'uuidv2' }),
      Joi.string().uuid({ version: 'uuidv2' }),
      Joi.string().guid({ version: 'uuidv3' }),
      Joi.string().uuid({ version: 'uuidv3' }),
      Joi.string().guid({ version: 'uuidv4' }),
      Joi.string().uuid({ version: 'uuidv4' }),
      Joi.string().guid({ version: 'uuidv5' }),
      Joi.string().uuid({ version: 'uuidv5' }),
      Joi.string().uuid({ version: ['uuidv1', 'uuidv2', 'uuidv3', 'uuidv4', 'uuidv5'] }),
      Joi.string().hex(),
      Joi.string().hex({ byteAligned: true }),
      Joi.string().base64(),
      Joi.string().base64({ paddingRequired: false }),
      Joi.string().dataUri(),
      Joi.string().dataUri({ paddingRequired: false }),
      Joi.string().hostname(),
      Joi.string().normalize(),
      Joi.string().normalize('NFC'),
      Joi.string().normalize('NFD'),
      Joi.string().normalize('NFKC'),
      Joi.string().normalize('NFKD'),
      Joi.string().lowercase(),
      Joi.string().uppercase(),
      Joi.string().trim(),
      Joi.string()
        .trim()
        .trim(false),
      Joi.string()
        .trim()
        .trim(false)
        .trim(true),
      Joi.string().isoDate(),
      Joi.string().regex(/[0-9]/),
      Joi.string().regex(/[0-9]/, { name: 'a name' }),
      Joi.string()
        .regex(/[0-9]/)
        .guid({ version: 'uuidv1' }),
      Joi.string().min(5),
      Joi.string()
        .min(5)
        .min(3)
        .min(7),
      Joi.string().max(5),
      Joi.string()
        .max(5)
        .max(3)
        .max(7),
      Joi.string()
        .min(5)
        .max(3)
        .max(7)
        .min(3)
        .min(7)
        .max(5),
      Joi.string().length(12),
      Joi.string()
        .length(12)
        .length(3),
      Joi.string()
        .min(5)
        .max(3)
        .max(7)
        .min(3)
        .length(12)
        .min(7)
        .max(5),
      Joi.string()
        .max(12)
        .max(7)
        .length(7)
        .min(3)
        .length(7)
        .min(7)
    ]
    simpleSchemata.forEach(schema => {
      it(`handles the schema ${JSON.stringify(schema.describe())}`, function () {
        const result = string2oas(schema)
        console.log(result)
        console.log()
      })
    })

    it('balks at regex combinations', function () {
      const s = Joi.string()
        .regex(/[0-9]/)
        .regex(/[A-Z]/)
      string2oas
        .bind(null, s)
        .should.throw(
          `${string2oas.messageStart.combineRegexp} Joi.string().regex(/[0-9]/), Joi.string().regex(/[A-Z]/)`
        )
    })

    const flaggedRegexes = [/[0-9]/i, /[0-9]/m, /[0-9]/s, /[0-9]/u, /[0-9]/imsu]
    flaggedRegexes.forEach(fr => {
      it(`balks at regex with flag ${fr.source}`, function () {
        const s = Joi.string().regex(fr)
        string2oas.bind(null, s).should.throw(`${string2oas.messageStart.translateFlags} (Joi.string().regex(${fr}))`)
      })
    })

    it('balks at an inverted regex', function () {
      const re = /[0-9]/
      const s = Joi.string().regex(re, { invert: true })
      string2oas
        .bind(null, s)
        .should.throw(`${string2oas.messageStart.handleInvertedRegexp} (Joi.string().regex(${re}, {invert: true}))`)
    })

    it('balks at format combinations', function () {
      const s = Joi.string()
        .lowercase()
        .uuid()
      string2oas
        .bind(null, s)
        .should.throw(`${string2oas.messageStart.combineFormats} Joi.string().lowercase(), Joi.string().guid()`)
    })
  })
})
