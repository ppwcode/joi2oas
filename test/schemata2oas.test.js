/* eslint-env mocha */

const schemata2oas = require('../lib/schemata2oas')
const Joi = require('@hapi/joi')

describe('schemata2oas', function () {
  beforeEach(function () {
    schemata2oas.contract.verifyPostconditions = true
  })

  afterEach(function () {
    schemata2oas.contract.verifyPostconditions = false
  })

  it('handles an empty schemata object', function () {
    const param = {}
    const result = schemata2oas(param)
    console.log(result)
    console.log()
  })

  const simpleSchemata = [Joi.string()]
  simpleSchemata.forEach(schema => {
    it(`handles a schemata object with 1 schema ${JSON.stringify(schema.describe())}`, function () {
      const result = schemata2oas({ simple: schema })
      console.log(result)
      console.log()
    })
  })
})
