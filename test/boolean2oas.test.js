/* eslint-env mocha */

const boolean2oas = require('../lib/boolean2oas')
const Joi = require('@hapi/joi')

describe('boolean2oas', function () {
  beforeEach(function () {
    boolean2oas.contract.verifyPostconditions = true
  })

  afterEach(function () {
    boolean2oas.contract.verifyPostconditions = false
  })

  const simpleSchemata = [
    Joi.boolean(),
    Joi.boolean().truthy('duck'),
    Joi.boolean().truthy('duck', 'swan'),
    Joi.boolean().truthy(['duck', 'swan']),
    Joi.boolean().falsy('not a duck'),
    Joi.boolean().falsy('not a duck', 'not a swan'),
    Joi.boolean().falsy(['not a duck', 'not a swan']),
    Joi.boolean()
      .truthy(['duck', 'swan'])
      .falsy('not a duck', 'not a swan')
      .insensitive(false)
  ]
  simpleSchemata.forEach(schema => {
    it(`handles the schema ${JSON.stringify(schema.describe())}`, function () {
      const result = boolean2oas(schema)
      console.log(result)
      console.log()
    })
  })
})
