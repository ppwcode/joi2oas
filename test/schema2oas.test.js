/* eslint-env mocha */

const schema2oas = require('../lib/schema2oas')
const Joi = require('@hapi/joi')

describe('schema2oas', function () {
  beforeEach(function () {
    schema2oas.contract.verifyPostconditions = true
  })

  afterEach(function () {
    schema2oas.contract.verifyPostconditions = false
  })

  const simpleSchemata = [
    Joi.func(),
    Joi.boolean(),
    Joi.boolean().truthy('duck'),
    Joi.boolean().truthy('duck', 'swan'),
    Joi.boolean().truthy(['duck', 'swan']),
    Joi.boolean().falsy('not a duck'),
    Joi.boolean().falsy('not a duck', 'not a swan'),
    Joi.boolean().falsy(['not a duck', 'not a swan']),
    Joi.boolean()
      .truthy(['duck', 'swan'])
      .falsy('not a duck', 'not a swan')
      .insensitive(false),
    Joi.number(),
    Joi.string(),
    Joi.string().min(0),
    Joi.string().min(2),
    Joi.string().min(Number.MAX_SAFE_INTEGER),
    Joi.string().max(0),
    Joi.string().max(2),
    Joi.string().max(Number.MAX_SAFE_INTEGER),
    Joi.string()
      .min(2)
      .max(3),
    Joi.string()
      .min(2)
      .min(4) // MUDO require max of min, not last
      .min(3),
    Joi.string()
      .max(2) // MUDO require min of max, not last
      .max(4)
      .max(3),
    Joi.number(),
    Joi.any()
  ]
  simpleSchemata.forEach(schema => {
    it(`handles the schema ${JSON.stringify(schema.describe())}`, function () {
      const result = schema2oas(schema)
      console.log(result)
      console.log()
    })
  })
})
