/* eslint-env mocha */

const Joi = require('@hapi/joi')
const should = require('should')

describe('joi', function () {
  it('joi does an and when combining formats', function () {
    const s = Joi.string()
      .email()
      .token()
    const err1 = s.validate('piet@peeters.com').error.should.be.an.Error().obj
    err1.message.should.match(/alpha-numeric and underscore/)
    const err2 = s.validate('alpha_numeric').error.should.be.an.Error().obj
    err2.message.should.match(/valid email/)
  })

  it('joi does an or when combining options', function () {
    const s = Joi.string().ip({ version: ['ipv4', 'ipv6'] })
    should(s.validate('192.168.0.1').error).be.null()
    should(s.validate('2001:0db8:85a3:0000:0000:8a2e:0370:7334').error).be.null()
  })
  it("joi matches all regex's given", function () {
    const s = Joi.string()
      .regex(/[0-9]/m)
      .regex(/[a-z]/i)
    should(s.validate('5  5325 52').error).be.an.Error()
    should(s.validate('53\n43').error).be.an.Error()
    should(s.validate('jfushS').error).be.an.Error()
    should(s.validate('iohtAAAiwe53\n43').error).be.null()
  })
})
