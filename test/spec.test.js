/* eslint-env mocha */

const spec = require('../lib/spec')
const should = require('should')

const expected = ['joi.schema', 'joi.schemata', 'joi.rules', 'oas.boolean', 'oas.double', 'oas.integer', 'oas.number']

describe('spec', function () {
  it('carries the expected payload', function () {
    spec.should.be.an.Object()
    spec.joi.should.be.an.Object()
    spec.oas.should.be.an.Object()
  })
  expected.forEach(c => {
    it(`carries a Joi schema for ${c}`, function () {
      const subjectPath = c.split('.')
      const subject = subjectPath.reduce((acc, k) => {
        return acc[k]
      }, spec)

      subject.should.be.an.Object()
      subject.isJoi.should.be.true()
      should(spec.joi.schema.validate(subject).error).be.null()
    })
  })
})
