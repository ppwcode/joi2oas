/* eslint-env mocha */

const nub = require('../../lib/_util/nub')

const arrayItems = [
  'test',
  'test',
  1,
  2,
  3,
  4,
  undefined,
  1,
  Number.MIN_SAFE_INTEGER,
  Number.MAX_SAFE_INTEGER,
  null,
  -1,
  false,
  1,
  null,
  undefined
]
const expected = ['test', 1, 2, 3, 4, undefined, Number.MIN_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, null, -1, false]

function mapper (e) {
  return { value1: { value2: e } }
}
describe('_util/nub', function () {
  it('removes duplicates without an eql', async function () {
    const result = nub(arrayItems)
    console.log(result)
    result.should.eql(expected)
  })

  it('removes duplicates with an eql 1', async function () {
    const objects = arrayItems.map(mapper)
    const result = nub(
      objects,
      (e1, e2) => (!e1.value1 && e2.value2) || (e1.value1 && (!e2.value1 || e1.value1.value2 === e2.value1.value2))
    )
    console.log(result)
    result.should.eql(expected.map(mapper))
  })

  it('removes duplicates with an eql 2', async function () {
    const result = nub(
      arrayItems,
      (e1, e2) =>
        (e1 === undefined && e2 === undefined) ||
        (e1 === null && e2 === null) ||
        typeof e2 === 'string' ||
        (Number.isInteger(e2) && (e2 < 0 || e2 > 3))
    )
    console.log(result)
    result.should.eql(['test', 1, 2, 3, undefined, 1, null, false, 1])
  })
})
