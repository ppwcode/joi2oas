/* eslint-env mocha */

const number2oas = require('../lib/number2oas')
const Joi = require('@hapi/joi')

describe('number2oas', function () {
  beforeEach(function () {
    number2oas.contract.verifyPostconditions = true
    number2oas.minimum.contract.verifyPostconditions = true
    number2oas.maximum.contract.verifyPostconditions = true
    number2oas.multiples.contract.verifyPostconditions = true
    number2oas.prune.contract.verifyPostconditions = true
  })

  afterEach(function () {
    number2oas.prune.contract.verifyPostconditions = false
    number2oas.multiples.contract.verifyPostconditions = false
    number2oas.maximum.contract.verifyPostconditions = false
    number2oas.minimum.contract.verifyPostconditions = false
    number2oas.contract.verifyPostconditions = false
  })

  const simpleSchemata = [
    Joi.number(),
    Joi.number().integer(),
    Joi.number()
      .integer()
      .integer(),
    Joi.number().min(5),
    Joi.number().min(-5),
    Joi.number()
      .min(5)
      .min(-4),
    Joi.number()
      .min(-4)
      .min(5),
    Joi.number().greater(5),
    Joi.number().greater(-5),
    Joi.number()
      .greater(5)
      .greater(-4),
    Joi.number()
      .greater(-4)
      .greater(5),
    Joi.number()
      .positive()
      .negative(),
    Joi.number()
      .min(5)
      .min(-4)
      .greater(5),
    Joi.number()
      .min(-4)
      .min(5)
      .greater(-4),
    Joi.number().positive(),
    Joi.number()
      .positive()
      .min(-4),
    Joi.number()
      .positive()
      .min(5),
    Joi.number()
      .positive()
      .greater(-4),
    Joi.number()
      .positive()
      .greater(5),
    Joi.number()
      .positive()
      .greater(0),
    Joi.number().max(5),
    Joi.number().max(-5),
    Joi.number()
      .max(5)
      .max(-4),
    Joi.number()
      .max(-4)
      .max(5),
    Joi.number().less(5),
    Joi.number().less(-5),
    Joi.number()
      .less(5)
      .less(-4),
    Joi.number()
      .less(-4)
      .less(5),
    Joi.number()
      .negative()
      .negative(),
    Joi.number()
      .max(5)
      .max(-4)
      .less(5),
    Joi.number()
      .max(-4)
      .max(5)
      .less(-4),
    Joi.number().negative(),
    Joi.number()
      .negative()
      .max(-4),
    Joi.number()
      .negative()
      .max(5),
    Joi.number()
      .negative()
      .less(-4),
    Joi.number()
      .negative()
      .less(5),
    Joi.number()
      .negative()
      .less(0),
    Joi.number()
      .negative()
      .min(-4),
    Joi.number()
      .negative()
      .min(5),
    Joi.number()
      .negative()
      .greater(-4),
    Joi.number()
      .negative()
      .greater(5),
    Joi.number()
      .negative()
      .greater(0),
    Joi.number()
      .positive()
      .max(-4),
    Joi.number()
      .positive()
      .max(5),
    Joi.number()
      .positive()
      .less(-4),
    Joi.number()
      .positive()
      .less(5),
    Joi.number()
      .min(-4)
      .max(5),
    Joi.number()
      .min(5)
      .max(-4),
    Joi.number()
      .integer()
      .min(-4)
      .max(5)
      .positive(),
    Joi.number().port(),
    Joi.number()
      .port()
      .greater(1024),
    Joi.number()
      .port()
      .greater(1024)
      .max(10000),
    Joi.number()
      .port()
      .greater(1024)
      .max(66000),
    Joi.number()
      .port()
      .min(-4)
      .max(5)
      .positive(),
    Joi.number()
      .port()
      .integer()
      .min(-4)
      .max(5)
      .positive(),
    Joi.number().precision(2),
    Joi.number().precision(0),
    Joi.number().precision(-2),
    Joi.number()
      .integer()
      .precision(2),
    Joi.number().multiple(1),
    Joi.number().multiple(2),
    Joi.number().multiple(Math.E),
    Joi.number()
      .multiple(2)
      .multiple(3),
    Joi.number()
      .multiple(2)
      .multiple(2)
  ]
  simpleSchemata.forEach(schema => {
    it(`handles the schema ${JSON.stringify(schema.describe())}`, function () {
      const result = number2oas(schema)
      console.log(result)
      console.log()
    })
  })
})
