const Joi = require('@hapi/joi')

const schema = Joi.object()
  .schema()
  .required()
const noArgRule = Joi.object({
  name: Joi.string()
    .valid('integer', 'positive', 'negative', 'port')
    .required()
})
const argNumberRule = Joi.object({
  name: Joi.string()
    .valid('multiple', 'min', 'greater', 'max', 'less', 'precision')
    .required(),
  arg: Joi.number().required()
})

const numberCommon = Joi.object({
  minimum: Joi.number().optional(),
  exclusiveMinimum: Joi.boolean().optional(),
  maximum: Joi.number().optional(),
  exclusiveMaximum: Joi.boolean().optional(),
  multipleOf: Joi.number()
    .positive()
    .optional()
})
  .with('exclusiveMinimum', 'minimum')
  .with('exclusiveMaximum', 'maximum')
const double = numberCommon
  .keys({
    type: Joi.string()
      .valid('number')
      .required(),
    format: Joi.string()
      .valid('double')
      .required(),
    'x-precision': Joi.number()
      .integer()
      .optional()
  })
  .required()
const integer = numberCommon
  .keys({
    type: Joi.string()
      .valid('integer')
      .required(),
    format: Joi.string()
      .valid('int64')
      .required()
  })
  .required()
const string = Joi.object({
  type: Joi.string()
    .valid('string')
    .required(),
  format: Joi.string().optional(),
  pattern: Joi.string().optional(),
  minLength: Joi.number().optional(),
  maxLength: Joi.number().optional()
}).required()

module.exports = {
  joi: {
    schema: schema,
    schemata: Joi.object()
      .pattern(/[a-zA-Z0-9-_]*/, schema)
      .required(),
    rules: Joi.array().items(Joi.alternatives().try(noArgRule, argNumberRule)) // add more as needed
  },
  oas: {
    boolean: Joi.object({
      type: Joi.string()
        .valid('boolean')
        .required()
    }).required(),
    double,
    integer,
    number: Joi.alternatives()
      .try(double, integer)
      .required(),
    string
  }
}
