const Contract = require('@toryt/contracts-iv')
const schema2oas = require('./schema2oas')
const schemataSchema = require('./spec').joi.schemata

/**
 * @param {object} schemata - map of Joi schemas
 * @returns {object} contains the same keys as `schemata`
 */
const schemata2oas = new Contract({
  pre: [schemata => !schemataSchema.validate(schemata, { convert: false }).error],
  post: [
    (schemata, result) => Object.keys(schemata).every(k => Object.keys(result).includes(k)),
    (schemata, result) => Object.keys(result).every(k => Object.keys(schemata).includes(k))
  ]
}).implementation(function schemata2oas (schemata) {
  return Object.keys(schemata).reduce((acc, k) => {
    acc[k] = schema2oas(schemata[k])
    return acc
  }, {})
})

module.exports = schemata2oas
