const Contract = require('@toryt/contracts-iv')
const schemaSchema = require('./spec').joi.schema
const boolean2oas = require('./boolean2oas')
const number2oas = require('./number2oas')

/**
 * Convert a [Joi schema](https://github.com/hapijs/joi/blob/v15.0.3/API.md) into an
 * [OpenAPI 3 schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#schemaObject).
 * The [Swagger tutorial](https://swagger.io/docs/specification/data-models/) might be easier to
 * understand.
 *
 * Based on [`joi-to-json-schema`](https://github.com/lightsofapollo/joi-to-json-schema/blob/master/src/index.js).
 *
 * `Joi.func()` schemata return `undefined`.
 *
 * For `string().min()` and `string().max()`, we always assume UTF-8.
 *
 * // MUDO each schema can have multiple examples: https://swagger.io/docs/specification/adding-examples/
 *
 * @param {object} schema - one Joi schema
 * @returns {object} representation of OpenAPI 3 schema for `schema`
 */
const schema2oas = new Contract({
  pre: [schema => !schemaSchema.validate(schema, { convert: false }).error],
  post: [
    (schema, result) => result === undefined || typeof result === 'object',
    (schema, result) => result !== undefined || (schema.describe().flags && schema.describe().flags.func)
  ]
}).implementation(function schema2oas (schema) {
  const d = schema.describe()
  const result = {}
  if (d.type === 'object' && d.flags && d.flags.func) {
    return undefined
  }
  if (d.type === 'boolean') {
    return boolean2oas(schema)
  }
  if (d.type === 'number') {
    return number2oas(schema)
  }
  if (d.type === 'string') {
    result.type = 'string'
    // MUDO insensitive -> enum
    if (d.rules) {
      const maxMin = d.rules
        .filter(r => r.name === 'min')
        .map(r => r.arg)
        .reduce((acc, arg) => (arg > acc ? arg : acc), 0) // max
      if (maxMin) {
        result.minLength = maxMin
      }
      const minMax = d.rules
        .filter(r => r.name === 'max')
        .map(r => r.arg)
        .reduce((acc, arg) => (arg < acc ? arg : acc), Number.MAX_SAFE_INTEGER) // min
      if (maxMin < Number.MAX_SAFE_INTEGER) {
        result.maxLength = minMax
      }
    }
  }
  return result
})

module.exports = schema2oas
