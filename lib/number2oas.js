const Contract = require('@toryt/contracts-iv')
const { joi, oas } = require('./spec')

const lowerBounds = ['min', 'greater', 'positive', 'port']
const positiveAsGreater = { name: 'greater', arg: 0 }
const portAsMin = { name: 'min', arg: 0 }
const degenerateMaxMin = {
  name: 'greater',
  arg: Number.NEGATIVE_INFINITY
}
const upperBounds = ['max', 'less', 'negative', 'port']
const negativeAsLess = { name: 'less', arg: 0 }
const portAsMax = { name: 'max', arg: 65535 }
const degenerateMinMax = {
  name: 'less',
  arg: Number.POSITIVE_INFINITY
}

const minimum = new Contract({
  pre: [rules => !joi.rules.validate(rules, { convert: false }).error],
  post: [
    (rules, result) => result === undefined || (!!result && typeof result === 'object'),
    (rules, result) => result === undefined || typeof result.minimum === 'number',
    (rules, result) =>
      result === undefined || result.exclusiveMinimum === undefined || result.exclusiveMinimum === true,
    (rules, result) => (rules && rules.filter(r => lowerBounds.includes(r.name)).length > 0) || result === undefined,
    (rules, result) =>
      result === undefined ||
      rules.some(
        r =>
          (r.name === 'min' && result.minimum === r.arg && !result.exclusiveMinimum) ||
          (r.name === 'greater' && result.minimum === r.arg && result.exclusiveMinimum) ||
          (r.name === 'positive' && result.minimum === 0 && result.exclusiveMinimum) ||
          (r.name === 'port' && result.minimum === 0 && !result.exclusiveMinimum)
      ),
    (rules, result) =>
      result === undefined ||
      result.minimum ===
        Math.max(
          ...rules
            .filter(r => lowerBounds.includes(r.name))
            .map(r => (r.name === 'positive' ? 0 : r.name === 'port' ? 0 : r.arg))
        )
  ]
}).implementation(function minimum (rules) {
  const maxMin = (rules || [])
    .filter(r => lowerBounds.includes(r.name))
    .map(r => (r.name === 'positive' ? positiveAsGreater : r.name === 'port' ? portAsMin : r))
    .reduce((acc, r) => (r.arg > acc.arg ? r : r.arg === acc.arg && r.name === 'greater' ? r : acc), degenerateMaxMin)
  if (maxMin.arg > degenerateMaxMin.arg || maxMin.name !== degenerateMaxMin.name) {
    return {
      minimum: maxMin.arg,
      exclusiveMinimum: maxMin.name === 'greater' || undefined
    }
  }
  return undefined
})

const maximum = new Contract({
  pre: [rules => !joi.rules.validate(rules, { convert: false }).error],
  post: [
    (rules, result) => result === undefined || (!!result && typeof result === 'object'),
    (rules, result) => result === undefined || typeof result.maximum === 'number',
    (rules, result) =>
      result === undefined || result.exclusiveMaximum === undefined || result.exclusiveMaximum === true,
    (rules, result) => (rules && rules.filter(r => upperBounds.includes(r.name)).length > 0) || result === undefined,
    (rules, result) =>
      result === undefined ||
      rules.some(
        r =>
          (r.name === 'max' && result.maximum === r.arg && !result.exclusiveMaximum) ||
          (r.name === 'less' && result.maximum === r.arg && result.exclusiveMaximum) ||
          (r.name === 'negative' && result.maximum === 0 && result.exclusiveMaximum) ||
          (r.name === 'port' && result.maximum === 65535 && !result.exclusiveMaximum)
      ),
    (rules, result) =>
      result === undefined ||
      result.maximum ===
        Math.min(
          ...rules
            .filter(r => upperBounds.includes(r.name))
            .map(r => (r.name === 'negative' ? 0 : r.name === 'port' ? 65535 : r.arg))
        )
  ]
}).implementation(function maximum (rules) {
  const minMax = (rules || [])
    .filter(r => upperBounds.includes(r.name))
    .map(r => (r.name === 'negative' ? negativeAsLess : r.name === 'port' ? portAsMax : r))
    .reduce((acc, r) => (r.arg < acc.arg ? r : r.arg === acc.arg && r.name === 'less' ? r : acc), degenerateMinMax)
  if (minMax.arg < degenerateMinMax.arg || minMax.name !== degenerateMinMax.name) {
    return {
      maximum: minMax.arg,
      exclusiveMaximum: minMax.name === 'less' || undefined
    }
  }
  return undefined
})

const multiples = new Contract({
  pre: [rules => !joi.rules.validate(rules, { convert: false }).error],
  post: [
    (rules, result) => result === undefined || typeof result === 'number',
    (rules, result) => result === undefined || result > 0,
    /* prettier-ignore */
    (rules, result) => !result === (!rules || rules.filter(r => r.name === 'multiple').length <= 0),
    (rules, result) =>
      !result ||
      result ===
        Array.from(new Set(rules.filter(r => r.name === 'multiple').map(r => r.arg))).reduce((acc, f) => acc * f, 1)
  ]
}).implementation(function multiples (rules) {
  const multiples = Array.from(new Set((rules || []).filter(r => r.name === 'multiple').map(r => r.arg)))
  if (multiples.length > 0) {
    return multiples.reduce((acc, f) => acc * f, 1)
  }
  return undefined
})

const prune = new Contract({
  pre: [obj => !!obj, obj => typeof obj === 'object'],
  post: [
    (obj, result) => obj === result,
    (obj, result) => Object.keys(result).every(k => result[k] !== undefined),
    (obj, result) => Object.keys(result).every(k => result[k] === obj[k]),
    (obj, result) =>
      Object.keys(obj)
        .filter(k => obj[k] !== undefined)
        .every(k => k in result)
  ]
}).implementation(function prune (obj) {
  Object.keys(obj).forEach(k => {
    if (obj[k] === undefined) {
      delete obj[k]
    }
  })
  return obj
})

/**
 * Convert a [Joi `number` schema](https://github.com/hapijs/joi/blob/v15.0.3/API.md#number---inherits-from-any) into
 * an [OpenAPI 3 schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#schemaObject).
 * The [Swagger tutorial](https://swagger.io/docs/specification/data-models/data-types/#numbers) might be easier to
 * understand.
 *
 * Since we are doing JavaScript, we know numbers are either OAS `int64` or `double`. It is impossible to have `int32`
 * or `float` numbers in JavaScript. `Joi.number().unsafe()` is ignored.
 *
 * With `Joi.number().min()`, `Joi.number().max()`, `Joi.number().greater()`, `Joi.number().less()`,
 * `Joi.number().positive()`,`Joi.number().negative()`, it is possible to describe empty intervals. We do not interfere
 * with that.
 *
 * OAS has no support for communicating the precision of floating point numbers, as expressed by
 * `Joi.number().precision()`. We add this as a
 * [Specification Extension](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#specificationExtensions)
 * `x-precision`, only for non-integer numbers. Note that a precision of `0` or less makes no sense, yet it is possible
 * to describe it with Joi. We do not interfere with that.
 *
 * @param {object} schema - one Joi `number` schema
 * @returns {object} representation of OpenAPI 3 schema for `schema`
 */
const number2oas = new Contract({
  pre: [
    schema => !joi.schema.validate(schema, { convert: false }).error,
    schema => schema.describe().type === 'number'
  ],
  post: [
    (schema, result) => !oas.number.validate(result, { convert: false }).error,
    (schema, result) => Object.keys(result).every(k => result[k] !== undefined),
    (schema, result) =>
      (result.type === 'integer' && result.format === 'int64') ||
      (result.type === 'number' && result.format === 'double'),
    (schema, result) =>
      (result.type === 'number') ===
      (!schema.describe().rules || schema.describe().rules.every(r => r.name !== 'integer' && r.name !== 'port')),
    (schema, result) =>
      result.minimum ===
      (schema.describe().rules && minimum(schema.describe().rules) && minimum(schema.describe().rules).minimum),
    (schema, result) =>
      result.exclusiveMinimum ===
      (schema.describe().rules &&
        minimum(schema.describe().rules) &&
        minimum(schema.describe().rules).exclusiveMinimum),
    (schema, result) =>
      result.maximum ===
      (schema.describe().rules && maximum(schema.describe().rules) && maximum(schema.describe().rules).maximum),
    (schema, result) =>
      result.exclusiveMaximum ===
      (schema.describe().rules &&
        maximum(schema.describe().rules) &&
        maximum(schema.describe().rules).exclusiveMaximum),
    (schema, result) => !('x-precision' in result) || result.type === 'number',
    (schema, result) =>
      !('x-precision' in result) ||
      result['x-precision'] === (schema.describe().flags && schema.describe().flags.precision),
    (schema, result) => result.multipleOf === (schema.describe().rules && multiples(schema.describe().rules))
  ]
}).implementation(function number2oas (schema) {
  const d = schema.describe()
  const isInteger = d.rules && d.rules.find(r => r.name === 'integer' || r.name === 'port')
  return prune({
    type: isInteger ? 'integer' : 'number',
    format: isInteger ? 'int64' : 'double',
    'x-precision': isInteger ? undefined : d.flags && d.flags.precision,
    ...minimum(d.rules),
    ...maximum(d.rules),
    multipleOf: multiples(d.rules)
  })
})

number2oas.minimum = minimum
number2oas.maximum = maximum
number2oas.multiples = multiples
number2oas.prune = prune

module.exports = number2oas
