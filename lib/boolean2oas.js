const Contract = require('@toryt/contracts-iv')
const { joi, oas } = require('./spec')

const oasSchema = { type: 'boolean' }

/**
 * Convert a [Joi `boolean` schema](https://github.com/hapijs/joi/blob/v15.0.3/API.md#boolean---inherits-from-any) into
 * an [OpenAPI 3 schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#schemaObject).
 * The [Swagger tutorial](https://swagger.io/docs/specification/data-models/data-types/#boolean) might be easier to
 * understand.
 *
 * `Joi.boolean()` supports additional values, that will be converted to `true` or `false` by the schema, with
 * `Joi.boolean().truthy()` and `Joi.boolean().falsy()` respectively, and case variants with
 * `Joi.boolean().insensitive()`. Although it might be interesting for your application to allow such values, and
 * convert them to boolean, we take the stance that the API documentation should only advertise strict data formats.
 * If we need a boolean, the user should give us a boolean. This means that `Joi.boolean().truthy()`,
 * `Joi.boolean().falsy()`, and `Joi.boolean().insensitive()` are discarded by this function.
 *
 * @param {object} schema - one Joi `boolean` schema
 * @returns {object} representation of OpenAPI 3 schema for `schema`
 */
const boolean2oas = new Contract({
  pre: [
    schema => !joi.schema.validate(schema, { convert: false }).error,
    schema => schema.describe().type === 'boolean'
  ],
  post: [(schema, result) => !oas.boolean.validate(result, { convert: false }).error]
}).implementation(function boolean2oas () {
  return oasSchema
})

module.exports = boolean2oas
