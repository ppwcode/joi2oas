const Contract = require('@toryt/contracts-iv')
const { joi, oas } = require('./spec')
const nub = require('./_util/nub')
const Hoek = require('@hapi/hoek')
/* istanbul ignore next */
if (!Array.prototype.flat) {
  // support Node 10 (or earlier) with flat polyfill
  require('array-flat-polyfill')
}

function nameNub (rules) {
  return nub(rules, (r1, r2) => r2.name === r1.name)
}

const ipVersions = ['ipv4', 'ipv6', 'ipvfuture']
const requiredOrOptional = ['required', 'optional']

const rule2format = {
  creditCard: () => ['credit_card'],
  alphanum: () => ['alpha-numeric'],
  token: () => ['token'],
  email: () => ['email'],
  ip: r => (r.arg.version || ipVersions).map(v => (requiredOrOptional.includes(r.arg.cidr) ? v + '-cidr' : v)),
  uri: () => ['uri'],
  guid: r => (r.arg && r.arg.version) || ['guid'],
  hex: () => ['hexadecimal'],
  base64: () => ['base64'],
  dataUri: () => ['data-uri'],
  hostname: () => ['hostname'],
  normalize: r => [r.arg],
  lowercase: () => ['lowercase'],
  uppercase: () => ['uppercase'],
  trim: () => ['trimmed'],
  isoDate: () => ['iso-8601']
}

// https://www.regextester.com/95309
const ipv4Start = '^(?:(?:^|\\.)(?:2(?:5[0-5]|[0-4]\\d)|1?\\d?\\d)){4}'
// ~ https://community.helpsystems.com/forums/intermapper/miscellaneous-topics/5acc4fcf-fa83-e511-80cf-0050568460e4?_ga=2.113564423.1432958022.1523882681-2146416484.1523557976
const ipv6Start =
  '^((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))'
const uuidBase = '[0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12}'
const uuid = `^${uuidBase}$`
// ~ https://rgxdb.com/r/1NUN74O6
const base64End = '(?:[a-zA-Z0-9+/]{4})*(?:|(?:[a-zA-Z0-9+/]{3}=)|(?:[a-zA-Z0-9+/]{2}==)|(?:[a-zA-Z0-9+/]===))$'

// noinspection SpellCheckingInspection
const patterns = {
  'alpha-numeric': '^[A-Za-z0-9]*$',
  token: '^\\w*$',
  // https://emailregex.com
  email:
    '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
  // https://www.regextester.com/95309
  ipv4: ipv4Start + '$',
  // + https://www.regextester.com/93987
  'ipv4-cidr': ipv4Start + '\\/([\\d]|[1-2][\\d]|3[0-2])$',
  ipv6: ipv6Start + '$',
  // + https://www.regextester.com/93988
  'ipv6-cidr': ipv6Start + '\\/([0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-8])$',
  // ~ snipplr.com/view/6889/regular-expressions-for-uri-validationparsing/
  uri:
    "^([a-zA-Z][a-zA-Z0-9+.-]*):(?:\\/\\/((?:(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:]|%[0-9A-F]{2})*))(\\3)@)?(?=(\\[[0-9A-F:.]{2,}]|(?:[a-zA-Z0-9-._~!$&'()*+,;=]|%[0-9A-F]{2})*))\\5(?::(?=(\\d*))\\6)?)(\\/(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/]|%[0-9A-F]{2})*))\\8)?|(\\/?(?!\\/)(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/]|%[0-9A-F]{2})*))\\10)?)(?:\\?(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/?]|%[0-9A-F]{2})*))\\11)?(?:#(?=((?:[a-zA-Z0-9-._~!$&'()*+,;=:@/?]|%[0-9A-F]{2})*))\\12)?$",
  // ~ https://regex101.com/r/hD8sJ8/3
  guid: `^\\{${uuidBase}}$`,
  uuidv1: uuid,
  uuidv2: uuid,
  uuidv3: uuid,
  uuidv4: uuid,
  uuidv5: uuid,
  hexadecimal: '^[0-9A-Fa-f]+$',
  // ~ https://rgxdb.com/r/1NUN74O6
  base64: `^${base64End}`,
  // ~ https://stackoverflow.com/questions/106179/regular-expression-to-match-dns-hostname-or-ip-address
  hostname: /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])$/,
  // no pattern for normalization, since I don't know what that is
  // impossible to have a UTF-8 compliant pattern for uppercase and lowercase in JavaScript
  'data-uri': `^(data:)([\\w\\/\\+]+);(charset=[\\w-]+|base64).*,${base64End}`,
  trimmed: '^[^\\s].*[^\\s]$',
  // https://www.myintervals.com/blog/2009/05/20/iso-8601-date-validation-that-doesnt-suck/
  'iso-8601': /^([+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24:?00)([.,]\d+(?!:))?)?(\17[0-5]\d([.,]\d+)?)?([zZ]|([+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/
}

const messageStart = {
  combineRegexp: 'Cannot combine regular expressions',
  handleInvertedRegexp: 'Cannot handle inverted regular expression',
  translateFlags: 'Cannot translate regular expression flags',
  combineFormats: 'Cannot combine formats'
}

/**
 * Convert a [Joi `string` schema](https://github.com/hapijs/joi/blob/v15.1.0/API.md#string---inherits-from-any) into
 * an [OpenAPI 3 schema](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#schemaObject).
 * The [Swagger tutorial](https://swagger.io/docs/specification/data-models/data-types/#string) might be easier to
 * understand.
 *
 * For `Joi.string().min()` and `Joi.string().max()`, we always assume UTF-8.
 *
 * `Joi.string().creditCard()`, `Joi.string().alphanum()`, `Joi.string().token()`, `Joi.string().email()`,
 * `Joi.string().ip()`, `Joi.string().uri()`, `Joi.string().uuid()` / `Joi.string().guid()`, `Joi.string().hex()`,
 * `Joi.string().base64()`, `Joi.string().dataUri()`, `Joi.string().normalize()`, `Joi.string().lowercase()`,
 * `Joi.string().uppercase()`, and `Joi.string().trim()` are all translated as unsupported OAS `format`, as this is
 * allowed by OAS. Most are also translated into a `pattern`. For `Joi.string().creditCard()` this is not possible. For
 * `Joi.string().email()` the pattern is only an approximation. `Joi.string().isoDate()` is translated this way too, as
 * OAS `format: 'date-time'` has second precision only, and is not the JavaScript ISO format. The format used allows,
 * e.g., for 30 days in February. There is no pattern for `Joi.string().ip()` `ipvfuture`, nor
 * `Joi.string().normalize()`, because I don't know what that they are. Do you? Please create a PR.
 * `Joi.string().uppercase()` and `Joi.string().lowercase()` are defined using the locale of the host environment,
 * taking into account UTF-8, and not only ASCII. It is impossible to produce a `RegExp` pattern for that in JavaScript.
 *
 * In some cases, using options, one Joi format can define variants. In the OAS `format`, we list them all, separated
 * with '|', as we do in the pattern. If `Joi.string().regex()` is defined, it takes precedence.
 *
 * Whenever one of the above formats is defined, case-insensitivity is assumed.
 *
 * Joi schemata can combine several of the above format definitions. The result is mostly a schema for which there
 * cannot be a valid value. In general, such combinations do not make sense (and should be forbidden by Joi, but they
 * are not). We do not support such combinations, and throw an error.
 *
 * Flags specified in `Joi.string().regex()` are ignored. Joi schemata can combine several `Joi.string().regex()`
 * definitions, and values must match each (AND). In OAS, we can only specify one. Combining several in general is too
 * difficult. We do not support such combinations, and throw an error. The same applies to negating a pattern in
 * general (`{invert: true}`). In OAS, we cannot specify flags. We do not support patterns with flags, and throw an
 * error.
 *
 * `Joi.string().min()`, `Joi.string().max()` and `Joi.string().length()` can be combined in impossible intervals. That
 * is not our concern. We are just here to translated.
 *
 * `Joi.string().truncate()` and `Joi.string().replace()` are conversion options, and are not type descriptions. They
 * are disregarded by this code.
 *
 * The OAS `type: string` `format`s `date`, `date-time`, `password`, `byte` and `binary` are produced by other
 * Joi schemata.
 *
 * @param {object} schema - one Joi `string` schema
 * @returns {object} representation of OpenAPI 3 schema for `schema`
 */
const string2oas = new Contract({
  pre: [
    schema => !joi.schema.validate(schema, { convert: false }).error,
    schema => schema.describe().type === 'string'
  ],
  post: [
    (schema, result) => !oas.string.validate(result, { convert: false }).error,
    (schema, result) => Object.keys(result).every(k => result[k] !== undefined),
    // TODO this needs loads of helper functions
    // when do we have a minLength
    (schema, result) =>
      (result.minLength === undefined) ===
      (!schema.describe().rules ||
        Math.max(
          ...schema
            .describe()
            .rules.filter(r => r.name === 'min' || r.name === 'length')
            .map(r => r.arg)
        ) <= 0),
    // what is the minLength when we have it
    (schema, result) =>
      result.minLength === undefined ||
      result.minLength ===
        Math.max(
          ...schema
            .describe()
            .rules.filter(r => r.name === 'min' || r.name === 'length')
            .map(r => r.arg)
        ),
    // when do we have a maxLength
    (schema, result) =>
      (result.maxLength === undefined) ===
      (!schema.describe().rules ||
        Math.min(
          ...schema
            .describe()
            .rules.filter(r => r.name === 'max' || r.name === 'length')
            .map(r => r.arg)
        ) >= Number.MAX_SAFE_INTEGER),
    // what is the maxLength when we have it
    (schema, result) =>
      result.maxLength === undefined ||
      result.maxLength ===
        Math.min(
          ...schema
            .describe()
            .rules.filter(r => r.name === 'max' || r.name === 'length')
            .map(r => r.arg)
        ),
    // when do we have a pattern
    (schema, result) =>
      (result.pattern === undefined) ===
      (!schema.describe().rules ||
        (!schema.describe().rules.find(r => r.name === 'regex') &&
          !nameNub(schema.describe().rules.filter(r => r.name in rule2format))
            .map(r => rule2format[r.name](r))
            .flat()
            .find(f => f in patterns))),
    // the pattern comes from regex if there is one
    (schema, result) =>
      result.pattern === undefined ||
      !schema.describe().rules.find(r => r.name === 'regex') ||
      result.pattern === schema.describe().rules.find(r => r.name === 'regex').arg.pattern.source,
    // the pattern comes from formats if there is no regex
    (schema, result) =>
      result.pattern === undefined ||
      schema.describe().rules.find(r => r.name === 'regex') ||
      result.pattern ===
        schema
          .describe()
          .rules.filter(r => r.name in rule2format) /* exactly 1 */
          .map(r => rule2format[r.name](r))
          .flat()
          .map(f => patterns[f])
          .filter(p => p)
          .join('|'), // TODO should be independent of order

    // when do we have a format
    (schema, result) =>
      (result.format === undefined) ===
      (!schema.describe().rules || !schema.describe().rules.find(r => r.name in rule2format)),
    // if we have a format, what is it
    (schema, result) =>
      result.format === undefined ||
      Hoek.deepEqual(
        result.format.split('|'),
        nameNub(schema.describe().rules.filter(r => r.name in rule2format))
          .map(r => rule2format[r.name](r))
          .flat()
      ),
    // non-nominal
    // no nominal end when there is more than 1 regex
    schema => !schema.describe().rules || schema.describe().rules.filter(r => r.name === 'regex').length <= 1,
    // no nominal end when there is regex with option invert = true
    schema =>
      !schema.describe().rules ||
      !schema.describe().rules.find(r => r.name === 'regex') ||
      !schema.describe().rules.filter(r => r.name === 'regex')[0].arg.invert,
    // no nominal end when there is regex with flags
    schema =>
      !schema.describe().rules ||
      !schema.describe().rules.find(r => r.name === 'regex') ||
      !schema.describe().rules.filter(r => r.name === 'regex')[0].arg.flags,
    // no nominal end when there is more than one format
    schema =>
      !schema.describe().rules || nameNub(schema.describe().rules.filter(r => r.name in rule2format)).length <= 1
  ],
  exception: [
    (schema, exc) => exc instanceof Error,
    (schema, exc) => exc.message,
    (schema, exc) => Object.keys(messageStart).some(m => exc.message.startsWith(messageStart[m])),
    schema => schema.describe().rules,
    (schema, exc) =>
      !exc.message.startsWith(messageStart.combineRegexp) ||
      schema.describe().rules.filter(r => r.name === 'regex').length > 1,
    (schema, exc) =>
      !exc.message.startsWith(messageStart.handleInvertedRegexp) ||
      schema.describe().rules.find(r => r.name === 'regex' && r.arg.invert),
    (schema, exc) =>
      !exc.message.startsWith(messageStart.translateFlags) ||
      schema.describe().rules.find(r => r.name === 'regex' && r.arg.pattern.flags),
    (schema, exc) =>
      !exc.message.startsWith(messageStart.combineFormats) ||
      new Set(
        schema
          .describe()
          .rules.map(r => r.name)
          .filter(n => n in rule2format)
      ).size > 1
  ]
}).implementation(function string2oas (schema) {
  const d = schema.describe()
  const result = { type: 'string' }
  if (d.rules) {
    const regexs = d.rules.filter(r => r.name === 'regex').map(r => r.arg)
    if (regexs.length > 1) {
      throw new Error(
        `${messageStart.combineRegexp} ${regexs.map(regex => `Joi.string().regex(${regex.pattern})`).join(', ')}`
      )
    }
    if (regexs.length > 0) {
      if (regexs[0].invert) {
        throw new Error(
          `${messageStart.handleInvertedRegexp} (Joi.string().regex(${regexs[0].pattern}, {invert: true}))`
        )
      }
      if (regexs[0].pattern.flags) {
        throw new Error(`${messageStart.translateFlags} (Joi.string().regex(${regexs[0].pattern}))`)
      }
      result.pattern = regexs[0].pattern.source
    }
    const formats = nameNub(d.rules.filter(r => r.name in rule2format))
    if (formats.length > 1) {
      throw new Error(`${messageStart.combineFormats} ${formats.map(r => `Joi.string().${r.name}()`).join(', ')}`)
    }
    if (formats.length > 0) {
      const oasFormats = rule2format[formats[0].name](formats[0])
      result.format = oasFormats.join('|')
      if (!result.pattern) {
        const oasPattern = oasFormats
          .filter(f => f in patterns)
          .map(f => patterns[f])
          .join('|')
        if (oasPattern) {
          result.pattern = oasPattern
        }
      }
    }
    const maxMin = Math.max(...d.rules.filter(r => r.name === 'min' || r.name === 'length').map(r => r.arg))
    if (maxMin > 0) {
      result.minLength = maxMin
    }
    const minMax = Math.min(...d.rules.filter(r => r.name === 'max' || r.name === 'length').map(r => r.arg))
    if (minMax < Number.MAX_SAFE_INTEGER) {
      result.maxLength = minMax
    }
  }
  return result
})

string2oas.rule2format = rule2format
string2oas.patterns = patterns
string2oas.messageStart = messageStart

module.exports = string2oas
