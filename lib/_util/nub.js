/**
 * @param {*} e1
 * @param {*} e2
 * @return {boolean}
 */
function defaultEql (e1, e2) {
  return e1 === e2
}

/**
 * Returns an array that has all the elements of `array`, in order, but with later duplicates removed.
 * Duplicates are defined by `eql`.
 *
 * @param {Array<*>} array
 * @param {function=} eql - 2 argument function, that returns truthy when only the first argument should be retained in
 *                         the array; the default is `===`
 * @return {boolean}
 */
function nub (array, eql) {
  return array.reduce((acc, e2) => {
    if (acc.some(e1 => (eql || defaultEql)(e1, e2))) {
      return acc
    }
    acc.push(e2)
    return acc
  }, [])
}

module.exports = nub
