# joi2oas

Convert Joi schemata into objects that can be saved as OAS schemata with references. These can that be stored as JSON or
YAML files.

## Target environment

Tested with the latest version of Node 10 and Node 12.

## Development

### IDE

JetBrains WebStorm settings are included and kept up-to-date by all developers.
